//
//  SignupVC.swift
//  WordWrap
//
//  Created by VeerChauhan on 28/12/18.
//  Copyright © 2018 Yug. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
class SignupVC: UIViewController {
    
    @IBOutlet weak var confirmPassTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var fullNameTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        //        // Do any additional setup after loading the view.
    }
    
    fileprivate func getParamsForRegistration(id : Int) -> [String : Any]{
        var paramDic = [String:Any]()
        paramDic["name"] = self.fullNameTF.text
        paramDic["password"] = self.passwordTF.text
        paramDic["confirm_Password"] = self.confirmPassTF.text
        paramDic["mobileNumber"] = self.mobileNumberTF.text
        paramDic["isOnline"] = true
        paramDic["user_id"] = id
        paramDic["user_registered_at"] = ServerValue.timestamp()
        paramDic["last_visit"] = ServerValue.timestamp()


        return paramDic
        
    }
    @IBAction func onClickSighnin(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func onClickSubmit(_ sender: UIButton) {
        if validateTextFields(){
            var id : Int = 1
            userdatabaseRef.observe(.value, with: { (snapshot: DataSnapshot!) in
                print("Got snapshot");
                print(snapshot.childrenCount)
                id = Int(snapshot.childrenCount)
            })
            userdatabaseRef.child(getId()).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists(){
                    print("user Already exist")
                    self.getAlertMessage(Title: Constant.APP_Name, Message: "user Already exist", ActionType: .info, styleType: .alert)
                }else{
                    print("user doesn't exist")
                   self.getAlertMessage(Title: Constant.APP_Name, Message: "Registration successful", ActionType: .info, styleType: .alert)
                    self.userdatabaseRef.child(self.getId()).setValue(self.getParamsForRegistration(id: id))
                    let homeVC = HomeVC()
                    self.navigationController?.pushViewController(homeVC, animated: true)
                }
            })
            
        }
    }
    fileprivate func getId() -> String {
        let mobile = self.mobileNumberTF.text!
        let key1  = "\(self.mobileNumberTF.text?.count ?? 1)"
        let key2  = "\(self.passwordTF.text?.count ?? 1)"
        
        let y = "Word" + mobile
        let x = y + key1 + key2 + "Wrap"
        return x
    }
}
extension SignupVC {
    fileprivate func validateTextFields() -> Bool {
        if (self.mobileNumberTF.text!.isEmpty) {
            getAlertMessage(Title: Constant.APP_Name, Message: "Mobile number can't be empty", ActionType: .info, styleType: .alert)
            return false
        }
        if (self.passwordTF.text!.isEmpty) {
             getAlertMessage(Title: Constant.APP_Name, Message: "Password can't beempty", ActionType: .info, styleType: .alert)
            return false
        }
        if (self.fullNameTF.text!.isEmpty) {
             getAlertMessage(Title: Constant.APP_Name, Message: "Full name can't be empty", ActionType: .info, styleType: .alert)
            return false
        }
        if (self.confirmPassTF.text!.isEmpty) {
             getAlertMessage(Title: Constant.APP_Name, Message: "Confirm password can't be empty", ActionType: .info, styleType: .alert)
            return false
        }
        return true
    }
}
extension String {
    func getCharecterAfterTrimming() -> String{
        var newCharecterSet = CharacterSet()
        newCharecterSet.insert(charactersIn: "@#$%^&*()")
        let x = self.trimmingCharacters(in:newCharecterSet)
        return x
    }
}
