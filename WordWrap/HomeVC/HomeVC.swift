//
//  HomeVC.swift
//  WordWrap
//
//  Created by VeerChauhan on 30/12/18.
//  Copyright © 2018 Yug. All rights reserved.
//

import UIKit

class HomeVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setnavigation(backButtonValue: true, navigationBarValue: false)
        self.title = "Home"
        self.view.backgroundColor = .red
        setupViewsForMainView()
        // Do any additional setup after loading the view.
    }
}
extension HomeVC{
    fileprivate func setupViewsForMainView(){
        
        let favVC = FavoriteVC()
        favVC.title = "Favourite"
        favVC.view.backgroundColor = UIColor.blue
        let profileVC = ProfileVC()
        profileVC.title = "Profile"
        profileVC.view.backgroundColor = UIColor.green
        let historyVC = HistoryVC()
        historyVC.title = "History"
        historyVC.view.backgroundColor = UIColor.yellow
        let controllers = [favVC, profileVC, historyVC]
        viewControllers = controllers

        favVC.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 0)
        profileVC.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 1)
        historyVC.tabBarItem = UITabBarItem(tabBarSystemItem: .history, tag: 2)

       self.tabBarController?.viewControllers = controllers.map { UINavigationController(rootViewController: $0)}
    }
}

extension UINavigationController {
    func setnavigation(backButtonValue : Bool , navigationBarValue : Bool){
        self.navigationController?.isNavigationBarHidden = navigationBarValue
        self.navigationItem.hidesBackButton = backButtonValue
    }
}
