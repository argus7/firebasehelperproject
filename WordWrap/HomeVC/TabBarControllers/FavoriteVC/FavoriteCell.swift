//
//  FavoriteCell.swift
//  WordWrap
//
//  Created by VeerChauhan on 31/12/18.
//  Copyright © 2018 Yug. All rights reserved.
//

import UIKit

class FavoriteCell: UITableViewCell {
    var message : String?
    var cellImage : UIImage?
    
    
    var messageView : UITextView = {
        var text = UITextView()
        text.translatesAutoresizingMaskIntoConstraints = false
        text.isScrollEnabled = false
        return text
    }()
    
    var mainImage : UIImageView = {
        var image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.blue
        self.addSubview(mainImage)
        self.addSubview(messageView)
        
        mainImage.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        mainImage.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        mainImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        mainImage.widthAnchor.constraint(equalToConstant: 100).isActive = true
        mainImage.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true

        mainImage.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true

        messageView.leftAnchor.constraint(equalTo: self.messageView.rightAnchor).isActive = true
        messageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        messageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        messageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        messageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        if let message = message {
            messageView.text = message
        }
        if let image = cellImage {
            mainImage.image = image
        }
    }
    
}
