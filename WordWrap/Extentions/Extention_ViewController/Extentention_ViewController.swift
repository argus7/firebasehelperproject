//
//  Extentention_ViewController.swift
//  Dejara
//
//  Created by TechGropse on 10/22/18.
//  Copyright © 2018 highBrowdelve.veer.Chauhan. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

/// this Enum property is created for Alert box Type
///
/// - cameraEvent: to upload any image
/// - InfoEvent: to show message

/// - ActionEvent: to perform any action with message
enum  AlertBoxEvent : Double {
    case camera = 0
    case info
    case action
}

enum dateComparedBy {
    case lessThan
    case greaterThan
    case equal
}

extension UIViewController {
    
    
    
    /// Get header Data for Authorization
    ///
    /// - Returns: HTTP header Type
    func getheaderData () -> HTTPHeaders {
        let udid = UserDefaults.standard.value(forKey: Constant.userDefaults.kDeviceID)
        let securityToken = UserDefaults.standard.value(forKey: Constant.userDefaults.kUserSecuirityToken)
        let headers: HTTPHeaders = [
            "device_id": (udid as? String ?? "").addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!,
            "security_token" : (securityToken as? String ?? "").addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        ]
        return headers
    }
    
    /// Set navigation Bar status (hidden or visible)
    ///
    /// - Parameter Value: True or False (Default is False)
    func setNavigationBarHidden(Value : Bool){
        self.navigationController?.setToolbarHidden(Value, animated: false)
    }
    
    /// Set Navigation Bar With Custom Color and and Custom Image
    ///
    /// - Parameters:
    ///   - imageName: Name of the image
    ///   - navigationBarColor: color for navigation Bar
    
    func setNavigationBar(imageName : UIImage, navigationBarColor : UIColor){
        self.navigationController?.navigationBar.backIndicatorImage = imageName
        self.navigationController?.navigationBar.backgroundColor =  navigationBarColor
    }
    
    
    /// Get Alert Message with type
    ///
    /// - Parameters:
    ///   - Title: App Name
    ///   - Message: Meassge for the event
    ///   - ActionType:  cameraEvent , InfoEvent , ActionEvent
    
    func getAlertMessage(Title : String , Message : String,ActionType : AlertBoxEvent ,styleType : UIAlertController.Style ) {
        let alertController =  UIAlertController.init(title: Title, message: Message, preferredStyle: styleType)
        let value = ActionType.rawValue
        print(value)

        switch ActionType {
        case .action:
            
            let alertActionOk =  UIAlertAction.init(title: "Ok", style: .default)
            let alertActionCancel  =  UIAlertAction.init(title: "Cancel", style: .default)
            alertController.addAction(alertActionOk)
            alertController.addAction(alertActionCancel)
            self.present(alertController,animated: true)
            break
        case .camera:
            let alertActionPhoto =  UIAlertAction.init(title: "Photo", style: .default)
            let alertActionCamera  =  UIAlertAction.init(title: "Camera", style: .default)
            alertController.addAction(alertActionPhoto)
            alertController.addAction(alertActionCamera)
            self.present(alertController,animated: true)
            break
        case .info:
            let alertActionOk =  UIAlertAction.init(title: "Ok", style: .default)
            alertController.addAction(alertActionOk)
            self.present(alertController,animated: true)
            break
        }
    }
}

extension UIViewController {
    func showImageSelectionAlert(picker : UIImagePickerController){
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("Camera")
            self.getImageByCamera(picker: picker)
        }
        let galleryAction = UIAlertAction(title: "Album", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("Album")
            self.getImageByPhotoStorage(picker: picker)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("No")
        }
        alertController.addAction(cameraAction)
        alertController.addAction(galleryAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension UIViewController {
    //MARK: - ALERT Controller and ImagePickerController Delegate
    func getImageByCamera(picker : UIImagePickerController){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        }
        else{
             self.getAlertMessage(Title: Constant.APP_Name, Message: "No camera", ActionType: .info, styleType: .alert)
        }
    }
    func getImageByPhotoStorage(picker : UIImagePickerController){
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    

    func checkViewController(viewControllerName : UIViewController) {
        
//        guard let controllers : [UIViewController] =  self.navigationController?.viewControllers  else{
//            self.navigationController?.pushViewController(viewControllerName, animated: true)
//        }
//        for controller in controllers {
//            if controller.isKind(of: viewControllerName )
//       
//
    }
    
    func checkNetworkConnection() -> Bool {
        return true
    }
    
    func validateDateAccordingly (pickedDate : Date, comparedDate: Date, caparedByType : dateComparedBy ) -> Bool{
        switch  caparedByType {
        case .equal:
            if pickedDate == comparedDate{
                return true
            }
            return false
        case .greaterThan :
            if pickedDate > comparedDate{
                return true
            }
            return false
        case .lessThan :
            if pickedDate < comparedDate{
                return true
            }
            return false
            
        }
    }
    
    func savetoUserDefault(data: Any,key : String) {
        UserDefaults.standard.set(data, forKey: key)
        UserDefaults.standard.synchronize()
    }
}
