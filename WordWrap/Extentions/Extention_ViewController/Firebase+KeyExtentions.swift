//
//  Firebase+KeyExtentions.swift
//  WordWrap
//
//  Created by VeerChauhan on 30/12/18.
//  Copyright © 2018 Yug. All rights reserved.
//

import Foundation
import Firebase
import UIKit
extension UIViewController {
    var userdatabaseRef: DatabaseReference
    {
        let ref = Database.database().reference()
        return ref.child("WordWrap").child("wordwrap_user")
    }
    var providerDatabaseRef : DatabaseReference {
        let ref = Database.database().reference()
        return ref.child("WordWrap").child("wordwrap_provider")
    }
}

