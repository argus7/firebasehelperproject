//
//  Constant.swift
//  Dejara
//
//  Created by TechGropse on 10/30/18.
//  Copyright © 2018 highBrowdelve.veer.Chauhan. All rights reserved.
//

import Foundation
class Constant {
    static let APP_Name                                                               = "WordWrap"
    static let Base_URL                                                               = "http://auctionbuy.in/dejera/api/user/"
    static let GoogleAPI_Key                                                          = "AIzaSyCI32D4OxUR8-dyGgSO3DJ7lLYmZzmaS8I"

    
    struct API_Service {
        static let mobileLogin_API                                                    = "sendMobileOtp"
        static let verifyOTP_API                                                      = "verifyOtp"
        static let Home_API                                                           = "getcategory"
        static let getProfile_API                                                     = "getProfile"
        static let editProfile_API                                                    = "editProfile"
        static let uploadProfileImage_API                                             = "uploadUserProfileImage"
        static let productList_API                                                    = "productlist"
        static let category_API                                                       = "getcategory"
        static let productDetail_API                                                  = "productdetails"
        static let postAd_API                                                         = "addproduct"
        static let uploadAdImage_API                                                  = "uploadAdimages"
        static let requestProduct_API                                                 = "requestproduct"

        
    }
    struct userDefaults {
        
        static let kIsUseregistered                                                 =  "IsUseregistered"
        static let kDeviceToken                                                     =  "deviceToken"
        static let kDeviceID                                                        =  "deviceID"
        static let kUserName                                                        =  "userName"
        static let kUserMobileNumber                                                =  "userMobileNumber"
        static let kUserEmail                                                       =  "userEmail"
        static let kUserPassword                                                    =  "userPassword"
        static let kUserAddress                                                     =  "userAddress"
        static let kUserDob                                                         =  "userDob"
        static let kUserId                                                          =  "userID"
        static let kUserStatus                                                      =  "userStatus"
        static let kUserLoginType                                                   =  "loginType"
        static let kUserImage                                                       =  "userImage"
        static let kUserRegistered                                                  =  "isRegistered"
        static let kUserRemembered                                                  =  "isRemembered"
        static let kUserGender                                                      =  "userGender"
        static let kUserCountry                                                     =  "userCountry"
        static let kUserCity                                                        =  "userCity"
        static let kUserZipCode                                                     =  "zipCode"
        static let kUserCountryCode                                                 =  "countryCode"
        static let kUserContact                                                     =  "userContact"
        static let kUserSecuirityToken                                              =  "secuirityToken"
        static let kUserFirstLogin                                                  =  "userFirstLogin"
        static let kLatitude                                                        =  "latitude"
        static let kLongitude                                                       =  "longitude"
        static let kuserProfileUpdate                                               =  "userProfileUpdate"
        static let kuserRegion                                                      =  "userRegion"
        static let kuserType                                                        =  "userType"
        
        
        
        
    }
}
