//
//  Extention_UIView.swift
//  Dejara
//
//  Created by TechGropse on 10/22/18.
//  Copyright © 2018 highBrowdelve.veer.Chauhan. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    
    /// Set any View Corner Radius round
    func setCornerRoundCornerRadius() {
        self.layer.cornerRadius =  self.layer.frame.size.height/2
        self.clipsToBounds =  true
    }
    
    /// set any view corner radius depending upon your need
    ///
    /// - Parameter radius: value in CGFloat
    func setCornerradius(radius: CGFloat){
        self.layer.cornerRadius =  radius
        self.clipsToBounds =  true
    }
    
    /// Set any view Corner radius with border color and border width
    ///
    /// - Parameters:
    ///   - radius: radius in CG float
    ///   - borderColor: color in Rgb or Uicolor.anyColor
    ///   - borderWidth: width in Radius
    func setCornerRadiusWithBorderColor(radius : CGFloat , borderColor : UIColor , borderWidth : CGFloat) {
        self.layer.cornerRadius =  radius
        self.layer.borderColor =  borderColor.cgColor
        self.layer.borderWidth =  borderWidth
        self.clipsToBounds =  true
    }
    /// Set any view Corner radius with border color and border width,  shadow effect
    ///
    /// - Parameters:
    ///   - radius: radius in CGFloat
    ///   - borderColor: color in UIColor or RGB color
    ///   - borderWidth: width in Float
    ///   - shadowColor: color in uicolor, or RGB color
    ///   - shadowOpacity: opacity in float
    ///   - shadowRadius: radius in flaot
    func setCornerRadiusWithBorderColorAndShadow(radius : CGFloat , borderColor : UIColor , borderWidth : CGFloat, shadowColor : UIColor, shadowOpacity : Float, shadowRadius : CGFloat) {
        self.layer.cornerRadius =  radius
        self.layer.borderColor =  borderColor.cgColor
        self.layer.borderWidth =  borderWidth
        self.layer.shadowColor =  shadowColor.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity =  shadowOpacity
        self.layer.shadowRadius = shadowRadius
        self.clipsToBounds =  true
    }
    
    func setLeftToRightGradient(cornerRadius : CGFloat,leftColor : UIColor, rightColor : UIColor) {
        self.backgroundColor = UIColor.clear
        if self.layer.sublayers != nil {
            for gradient in self.layer.sublayers! {
                if ((gradient as? CAGradientLayer) != nil) {
                    gradient.removeFromSuperlayer()
                }
            }
        }
        let gradient: CAGradientLayer = CAGradientLayer()
      
        gradient.colors = [leftColor.cgColor, rightColor.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.cornerRadius = cornerRadius
        gradient.frame = self.bounds
        self.layer.insertSublayer(gradient, at: 0)
    }
 
}
extension UIView {
    class func loadFromNibNamed(nibNamed: String, frame : CGRect) -> UIView? {
        let tView = UINib(
            nibName: nibNamed,
            bundle: nil
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
        tView?.frame = frame
        return tView
    }
}
