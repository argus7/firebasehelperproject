//
//  UIColor_Extention.swift
//  Dejara
//
//  Created by TechGropse on 10/30/18.
//  Copyright © 2018 highBrowdelve.veer.Chauhan. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    open class var bakGroundColorGray: UIColor { get  {
        return UIColor(red: (233.0/256.0), green: (233.0/256.0), blue: (233.0/256.0), alpha: 1.0)
        }
    }
    open class var darkBlueCol: UIColor { get  {
        return UIColor(red: (0.0/256.0), green: (43.0/256.0), blue: (121.0/256.0), alpha: 1.0)
        }
    }
    open class var lightBlueCol: UIColor { get  {
        return UIColor(red: (1.0/256.0), green: (57.0/256.0), blue: (157.0/256.0), alpha: 1.0)
        }
    }
    
    open class var statusBarCol: UIColor { get  {
        return UIColor(red: (18.0/256.0), green: (121.0/256.0), blue: (213.0/256.0), alpha: 1.0)
        }
    }

}

