//
//  LoginVC.swift
//  WordWrap
//
//  Created by VeerChauhan on 28/12/18.
//  Copyright © 2018 Yug. All rights reserved.
//

import UIKit
import Firebase
class LoginVC: UIViewController {

    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickSignup(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickSubmitBtn(_ sender: UIButton) {
        
        
        userdatabaseRef.child(getId()).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists(){
                guard let dict = snapshot.value as? [String:Any] else {
                    print("Error")
                    return
                }
                print(snapshot)
                
                self.moveTohomeVC(dataDic: dict)
            }else{
                print("User don't exist")
                self.getAlertMessage(Title: Constant.APP_Name, Message: "User don't exist", ActionType: .info, styleType: .alert)
            }
        })
    }
    
    fileprivate func moveTohomeVC(dataDic : [String:Any]){
        let password = dataDic["password"] as? String ?? ""
        if (password == self.passwordTF.text!){
            let userID = "\(dataDic["user_id"]!)"
            let userMobile = "\(dataDic["user_id"]!)"

            print(userMobile)
            savetoUserDefault(data: userID, key: Constant.userDefaults.kUserId)
            savetoUserDefault(data: true, key: Constant.userDefaults.kIsUseregistered)
            savetoUserDefault(data: true, key: Constant.userDefaults.kIsUseregistered)

            let homeVC =  HomeVC()
            self.navigationController?.pushViewController(homeVC, animated: true)
            
            
        }
        else{
            self.getAlertMessage(Title: Constant.APP_Name, Message: "Password doesnot matched ", ActionType: .info, styleType: .alert)
            print("Password incorrect")
        }
    }
    fileprivate func getId() -> String {
        let mobile = self.mobileNumberTF.text!
        let key1  = "\(self.mobileNumberTF.text?.count ?? 1)"
        let key2  = "\(self.passwordTF.text?.count ?? 1)"
        let y = "Word" + mobile
        let x = y + key1 + key2 + "Wrap"
        return x
    }

}


